# OpenML dataset: Generation-8-Pokemon

https://www.openml.org/d/43749

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset contains information from all 400 Pokemon in generation eight. 
Content

No.: Pokedex number
Name: Pokemon name in english
Ability1: Pokemon ability
Ability2: Pokemon second ability (if available)
Hidden_Ability: A Pokemon's hidden ability (if available)
Type1: A Pokemon's typing (e.g. fire, water, flying, etc.)
Type2: A Pokemon's secondary typing (if available)
HP: Base HP stat
Att: Base Attack stat
Def: Base Defense Stat
S.Att: Base Special Attack stat
S.Def: Base Special Defense stat
Spd: Base Speed stat
Weight_kg: Pokemon's weight in kilogram
Height_m: Pokemon's height in meters
Weight_lbs: Pokemon's weight in pounds
Height_ft: Pokemon's height in feet
Cap_Rate: Pokemon's capture rate
Egg_Steps: Base number of steps for a Pokemon's egg to hatch

Acknowledgements
This information was scraped from

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43749) of an [OpenML dataset](https://www.openml.org/d/43749). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43749/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43749/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43749/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

